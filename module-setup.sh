#!/bin/bash
# -*- mode: shell-script; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# ex: ts=8 sw=4 sts=4 et filetype=sh

check() {
  return 255
}

depends() {
  echo 'ssh-client busybox'
}

install() {
  local tmpdir=$(mktemp -d --tmpdir dracut-acquire-key-over-ssh.XXXX)
  local genconf="${tmpdir}/acquire-key-over-ssh.conf"
  local installconf="/etc/acquire-key-over-ssh.conf"

  [[ -z "${keyserver_port}" ]] && keyserver_port=22

  echo -e "#!/bin/bash\n\n" >> "$genconf"
  echo "keyserver_interface='${keyserver_interface}'" >> "$genconf"
  echo "keyserver_host='$keyserver_host'" >> "$genconf"
  echo "keyserver_user='$keyserver_user'" >> "$genconf"
  echo "keyserver_port='$keyserver_port'" >> "$genconf"
  echo "client_ssh_keys_device='${client_ssh_keys_device}'" >> "$genconf"
  echo "client_ssh_keys_mountpoint='$client_ssh_keys_mountpoint'" >> "$genconf"
  echo "client_identity_file='$client_identity_file'" >> "$genconf"
  echo "shred_keys_after_use='$shred_keys_after_use'" >> "$genconf"

  inst_hook initqueue/settled 99 "$moddir/start-ssh-acquire.sh"
  inst_script "$moddir/poll-ssh-acquire.sh" "/bin/poll-ssh-acquire.sh"
  inst_simple "/root/.ssh/known_hosts"
  inst_simple "/root/.ssh/id_rsa.pub"
  inst_simple "/root/.ssh/id_rsa"
  inst "$genconf" "$installconf"

  rm -rf $tmpdir
}
