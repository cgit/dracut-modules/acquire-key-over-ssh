Acquire Key Over Ssh
--------------------


This Dracut module allows the initramfs to obtain the decryption key for the
rootfs over an SSH connection. This can be useful if the machine is headless.

Using this in a setup of two machines that can unlock eachother means that the
encryption keys never need to live persistently on an unencrypted device.
