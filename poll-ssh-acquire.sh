#!/bin/bash

# This script waits for the network to come online and then will set the system time.
#
# This is useful because my system motherboard does not save the system time
# for some reason.

lockfile=/tmp/acquire-ssh-key-lock
timeout=600
count=0

. /etc/acquire-key-over-ssh.conf

# Wait for the given interface to come up and for a route to
# be added.
echo -n "Waiting for interface $keyserver_interface ... "
while ! ( ip route list dev "$keyserver_interface" &>/dev/null ) ; do
  if [[ "$count" -eq "$timeout" ]] ; then
    echo "Timedout waiting for $keyserver_interface. Exiting."
    exit 1
  fi

  echo -n "."
  sleep 0.1
  count=$((count + 1))
done
echo -e "\ndone"

# Amount of tries
tries=10

# If the ssh-key is on a device, we have to mount that device before
# we can do anything.
mounted=0

if [[ ! -z "$client_ssh_keys_device" ]] ; then
  if [[ -z "$client_ssh_keys_mountpoint" ]] ; then
    echo "client_ssh_keys_device requires client_ssh_keys_mountpoint to be set!"
    return 1
  fi

  count=0
  while true ; do
    if [[ "$count" -eq "$tries" ]] ; then
      echo "Failed to mount $client_ssh_keys_device on $client_ssh_keys_mountpoint after $tries tries"
      exit 1
    fi
    mkdir -p "$client_ssh_keys_mountpoint"
    echo "mounting $client_ssh_keys_device on $client_ssh_keys_mountpoint"
    mount "$client_ssh_keys_device" "$client_ssh_keys_mountpoint"
    ec="$?"
    if [[ "$ec" -eq 0 ]] ; then
      break
    fi
    sleep 1
    count=$((count + 1))
  done
  count=0

  mounted=1
fi

# Default identity file is root's identity file.
if [[ -z "$client_identity_file" ]] ; then
  client_identity_file=/root/.ssh/id_rsa
fi

if [[ ! -f "$client_identity_file" ]] ; then
  echo "$client_identity_file does not exist."
  exit 1
fi

# The main loop. This is going to try repeatedly 10 times to acquire the
# decryption key. If it fails each time, it will give up and someone
# will have to manually unlock the device.
count=0
acquired=0
while /bin/true ; do
  if [[ "$count" -eq "$tries" ]] ; then
    echo "Unable to connect to $keyserver_user@$keyserver_host after 10 tries."
    break;
  fi

  echo "Trying $keyserver_user@$keyserver_host ..."
  echo ssh -i "$client_identity_file" "$keyserver_user@$keyserver_host" -p "$keyserver_port"

  ssh -i "$client_identity_file" "$keyserver_user@$keyserver_host" -p "$keyserver_port" > /tmp/enc-key

  ec="$?"
  if [[ "$ec" -eq 0 ]] ; then
    acquired=1
    break;
  else
    echo "Non-zero exit code ec=$ec"
  fi

  sleep 1
  count=$((count + 1)) 
done

# Shred the keys and unmount the filessystems if needed.
if [[ "$shred_keys_after_use" -eq 1 ]] ; then
  echo "shred $client_identity_file"
  shred -u "$client_identity_file"
fi

if [[ "$mounted" -eq "1" ]] ; then
  echo "unmount client_ssh_keys_mountpoint"
  umount "$client_ssh_keys_mountpoint"
fi

# If the key was acquired, send it to systemd.
if [[ "$acquired" -eq 1 ]] ; then
  echo "Key acquired. Stored in /tmp/enc-key."

  socket_file=$(cat /run/systemd/ask-password/ask.* | grep -E '^Socket' | cut -d'=' -f2)
  echo "running: /lib/systemd/systemd-reply-password 1 $socket_file < /tmp/enc-key"
  /lib/systemd/systemd-reply-password 1 "$socket_file" < /tmp/enc-key
else
  echo "Failed to acquire key."
fi

# Shred the encryption key for good measure. Probably not necessary.
shred /tmp/enc-key
